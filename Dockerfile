FROM ubuntu:23.04

# Define working directory.
WORKDIR /home/ubuntu

# Installing openssh-client
RUN apt-get update && apt-get -y upgrade && apt-get install -y curl unzip jq

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

RUN unzip awscliv2.zip

RUN ./aws/install

RUN aws --version


# Define default command.
CMD ["bash"]